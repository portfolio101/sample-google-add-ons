function filterTwitter(rKeywords)
{
	// Trigger event to check content
	setTimeout(function(){
		getUserContentTwitter(rKeywords);
	},3000);
	$(window).scroll(function() {
	    clearTimeout($.data(this, 'scrollTimer'));
	    $.data(this, 'scrollTimer', setTimeout(function() {
	    	getUserContentTwitter(rKeywords);
	    }, 1000));
	});
}

function getUserContentTwitter(rKeywords)
{
	var arrayMessageContent = [];
	var arrayContentId = [];
	// Store descriptions and element id
	$('ol#stream-items-id li p').each(function(index) {
		arrayMessageContent.push($(this).text());
		arrayContentId.push($(this).parent().parent().parent().parent().attr('id'));
	});
	chkKeywordTwitter(arrayMessageContent, arrayContentId, rKeywords);
}

function chkKeywordTwitter(arrayMessageContent, arrayContentId, words)
{
	var arrayChangeContents = [];
	for (var i = 0; i < arrayMessageContent.length; i++) {
		// Find a keyword in a message/details (LOWERCASE)
		for (var x = 0; x < words.length; x++) {
			if(arrayMessageContent[i].toLowerCase().indexOf(words[x].toLowerCase()) !== -1){
				arrayChangeContents[arrayContentId[i]] = arrayMessageContent[i];
			}
		}
		
	}
	replaceContentTwitter(arrayChangeContents);
}

function replaceContentTwitter(arrayChangeContents)
{
	// Key = ID
	for (var key in arrayChangeContents){
		$('#' + key).empty(); // empty child element
	   	var z = document.createElement('div'); // create a node
	    var att = document.createAttribute("style");  // Create a "style" attribute
		att.value = "padding: 9px 12px;";   // Set the value of the "style" attribute
		z.setAttributeNode(att); 
		z.innerHTML = '<p style="padding: 9px 12px;">Description test!</p><a href="https://mcdelivery.com.ph/" target="_blank"><img class="animate-image-' + key + '"  style="opacity:0;margin: auto; width: 100%; display: block;" src="https://media.giphy.com/media/ScqCZtXodro1G/giphy.gif"></a>';
	    document.getElementById(key).appendChild(z); // Insert innerhtml to a node
	    addAnimate(key);
	}
}